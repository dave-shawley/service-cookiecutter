import logging
import unittest.mock
import uuid

import {{cookiecutter.package_name}}.logging


class LogContextTests(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.filter = {{cookiecutter.package_name}}.logging.LogContextInjector()
        self.record = logging.makeLogRecord({})
        self.token = {{cookiecutter.package_name}}.logging.context.set({})

    def tearDown(self):
        {{cookiecutter.package_name}}.logging.context.reset(self.token)

    def test_that_correlation_id_is_always_set(self):
        self.filter.filter(self.record)
        self.assertEqual('-', self.record.correlation_id)

    def test_that_correlation_id_is_read_from_context(self):
        log_context = {}
        {{cookiecutter.package_name}}.logging.context.set(log_context)
        log_context['correlation_id'] = 'CID'
        self.filter.filter(self.record)
        self.assertEqual('CID', self.record.correlation_id)

    def test_that_other_names_are_read_from_context(self):
        prop_name = uuid.uuid4().hex
        log_context = {}
        {{cookiecutter.package_name}}.logging.context.set(log_context)
        log_context[prop_name] = unittest.mock.sentinel.value
        self.filter.filter(self.record)
        self.assertEqual(unittest.mock.sentinel.value,
                         getattr(self.record, prop_name))

    def test_that_filter_does_not_reject_record(self):
        self.assertEqual(True, self.filter.filter(self.record))
