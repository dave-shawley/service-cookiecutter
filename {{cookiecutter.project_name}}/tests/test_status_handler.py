import json
import unittest.mock

import arrow
import tornado.log

from tests import helpers


class StatusEndpointTests(helpers.ApplicationTestCase):
    def setUp(self):
        self.test_started = arrow.get()
        super().setUp()

    def test_that_status_contains_environment_name(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)
        self.assertIn('application/json', response.headers['Content-Type'])

        body = json.loads(response.body.decode('utf-8'))
        self.assertEqual(self.app.settings['environment'], body['environment'])

    def test_that_status_contains_application_name(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)
        self.assertIn('application/json', response.headers['Content-Type'])

        body = json.loads(response.body.decode('utf-8'))
        self.assertEqual(self.app.settings['service'], body['application'])

    def test_that_status_contains_version(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)
        self.assertIn('application/json', response.headers['Content-Type'])

        body = json.loads(response.body.decode('utf-8'))
        self.assertEqual(self.app.settings['version'], body['version'])

    def test_that_status_contains_start_time(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)
        self.assertIn('application/json', response.headers['Content-Type'])

        body = json.loads(response.body.decode('utf-8'))
        now = arrow.get()
        then = arrow.get(body['started_at'])
        self.assertGreaterEqual(then, self.test_started)
        self.assertLessEqual(then, now)

    def test_that_status_contains_status(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)
        self.assertIn('application/json', response.headers['Content-Type'])

        body = json.loads(response.body.decode('utf-8'))
        self.assertEqual('ok', body['status'])

    def test_that_status_handler_is_not_logged(self):
        with unittest.mock.patch('tornado.log.access_log') as logger:
            self.fetch('/status')
        self.assertEqual(0, logger.info.call_count)
        self.assertEqual(0, logger.warning.call_count)
        self.assertEqual(0, logger.error.call_count)

    def test_that_status_handler_returns_503_when_unavailable(self):
        response = self.fetch('/status')
        self.assertEqual(200, response.code)

        self.app.status = 'not ok'
        response = self.fetch('/status')
        self.assertEqual(503, response.code)

    def test_that_status_handler_is_logged_when_failing(self):
        self.app.status = 'not ok'
        with self.assertLogs(tornado.log.access_log, 'ERROR'):
            response = self.fetch('/status')
            self.assertEqual(503, response.code)

    def test_that_status_handler_returns_status_when_unavailable(self):
        self.app.status = 'not ok'
        response = self.fetch('/status')
        self.assertEqual(503, response.code)

        body = json.loads(response.body.decode('utf-8'))
        self.assertEqual('not ok', body['status'])
