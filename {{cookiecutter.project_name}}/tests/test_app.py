import datetime
import logging
import re
import time
import unittest.mock

from cavy import testing
from ietfparse import headers
from sprockets.mixins.mediatype import content
from tornado import httputil, ioloop, log, web

import {{cookiecutter.package_name}}.logging
from {{cookiecutter.package_name}} import app, version


class EntryPointTests(testing.EnvironmentMixin, testing.PatchingMixin,
                      unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.make_app = self.patch('{{cookiecutter.package_name}}.app.make_app')
        self.http_run = self.patch('{{cookiecutter.package_name}}.app.sprockets.http.run')
        self.log_config = self.patch('{{cookiecutter.package_name}}.app.logging.config')

    def test_that_http_run_is_called(self):
        app.entry_point()
        defaults = {'number_of_procs': 0, 'xheaders': True}
        self.http_run.assert_called_once_with(
            self.make_app, defaults, log_config=self.log_config.return_value)

    def test_that_http_run_uses_number_of_procs(self):
        self.setenv('NUMBER_OF_PROCS', '42')
        app.entry_point()
        defaults = {'number_of_procs': 42, 'xheaders': True}
        self.http_run.assert_called_once_with(
            self.make_app, defaults, log_config=self.log_config.return_value)


class MakeAppTests(testing.PatchingMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.app_cls = self.patch('{{cookiecutter.package_name}}.app.Application')

    def test_that_application_is_created(self):
        app.make_app()
        self.app_cls.assert_called_once_with()

    def test_that_application_is_returned(self):
        obj = app.make_app()
        self.assertIs(self.app_cls.return_value, obj)


class ApplicationTests(testing.EnvironmentMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.json_content_type = headers.parse_content_type('application/json')

    def create_application(self) -> app.Application:
        obj = app.Application()
        iol = ioloop.IOLoop.current()
        obj.start(iol)
        if getattr(obj, 'statsd', None):
            obj.statsd._tcp_reconnect_sleep = 0.1

        def stop_application():
            obj.stop(iol, shutdown_limit=0.5, wait_timeout=0.05)
            iol.start()

        self.addCleanup(stop_application)

        return obj

    def test_that_environment_stored_in_settings(self):
        self.setenv('ENVIRONMENT', 'whatever')
        obj = self.create_application()
        self.assertEqual('whatever', obj.settings['environment'])

    def test_that_service_stored_in_settings(self):
        obj = self.create_application()
        self.assertEqual('{{cookiecutter.project_name}}', obj.settings['service'])

    def test_that_user_agent_stored_in_settings(self):
        obj = self.create_application()
        self.assertEqual(f'{{cookiecutter.project_name}}/{version}', obj.settings['user_agent'])

    def test_that_version_stored_in_settings(self):
        obj = self.create_application()
        self.assertEqual(version, obj.settings['version'])

    def test_that_json_is_registered(self):
        obj = self.create_application()
        settings = content.get_settings(obj)
        self.assertIn(self.json_content_type, settings.available_content_types)

    def test_that_json_is_default_content_type(self):
        obj = self.create_application()
        settings = content.get_settings(obj)
        self.assertEqual('application/json', settings.default_content_type)

    def test_that_sentry_is_enabled_if_dsn_is_defined(self):
        self.setenv('ENVIRONMENT', 'staging')
        self.setenv('SENTRY_DSN', 'http://public:private@sentry.io/project')

        obj = self.create_application()
        self.assertIsNotNone(obj.sentry_client)
        self.assertIn('{{cookiecutter.package_name}}', obj.sentry_client.include_paths)

    def test_that_sentry_is_disabled_if_dsn_is_not_defined(self):
        self.unsetenv('SENTRY_DSN')
        obj = self.create_application()
        self.assertIsNone(obj.sentry_client)

    def test_that_package_in_sentry_include_path(self):
        self.setenv('ENVIRONMENT', 'staging')
        self.setenv('SENTRY_DSN', 'http://public:private@sentry.io/project')

        obj = self.create_application()
        self.assertIsNotNone(obj.sentry_client)
        self.assertIn('{{cookiecutter.package_name}}', obj.sentry_client.include_paths)

    def test_that_version_is_set_in_sentry_client(self):
        self.setenv('ENVIRONMENT', 'staging')
        self.setenv('SENTRY_DSN', 'http://public:private@sentry.io/project')

        obj = self.create_application()
        self.assertIsNotNone(obj.sentry_client)
        self.assertEqual(version, obj.sentry_client.release)

    def test_that_environment_is_set_in_sentry_client(self):
        self.setenv('ENVIRONMENT', 'staging')
        self.setenv('SENTRY_DSN', 'http://public:private@sentry.io/project')

        obj = self.create_application()
        self.assertIsNotNone(obj.sentry_client)
        self.assertEqual('staging', obj.sentry_client.environment)

    def test_that_sentry_is_disabled_in_development(self):
        self.setenv('ENVIRONMENT', 'development')
        self.setenv('SENTRY_DSN', 'http://public:private@sentry.io/project')
        obj = self.create_application()
        self.assertIsNone(obj.sentry_client)

    def test_that_statsd_is_installed(self):
        obj = self.create_application()
        self.assertIsNotNone(obj.statsd)

    def test_that_statsd_configured_for_tcp(self):
        obj = self.create_application()
        self.assertTrue(obj.statsd._tcp)

    def test_that_statsd_path_is_correct(self):
        self.setenv('ENVIRONMENT', 'whatever')
        obj = self.create_application()
        self.assertEqual('applications.{{cookiecutter.project_name}}.whatever',
                         obj.statsd._namespace)

    def test_that_log_request_uses_expected_format(self):
        request = httputil.HTTPServerRequest('GET', '/search?q=42')
        request.remote_ip = '1.1.1.1'
        request._start_time = time.time()
        request.connection = unittest.mock.Mock()

        obj = self.create_application()
        handler = web.RequestHandler(obj, request)

        with self.assertLogs(log.access_log) as context:
            obj.log_request(handler)

        when = datetime.datetime.fromtimestamp(request._start_time,
                                               datetime.timezone.utc)
        expected_message = re.compile(
            r'^%s - - %s "%s %s %s" %d - "-" "-" \(secs:([^)]*)\)' %
            (request.remote_ip,
             re.escape(
                 when.strftime('[%d/%b/%Y:%H:%M:%S %z]')), request.method,
             re.escape(request.uri), request.version, handler.get_status()))
        message = context.records[0].getMessage()
        match = expected_message.match(message)
        if match is None:
            self.fail(f'"{message}" did not match "{expected_message}"')
        try:
            float(match.group(1))
        except ValueError:
            self.fail(f'Expected {match.group(1)} to be a float')

    def test_that_log_request_uses_correct_log_level(self):
        expectations = {
            200: logging.INFO,
            303: logging.INFO,
            400: logging.WARNING,
            404: logging.WARNING,
            500: logging.ERROR,
            599: logging.ERROR,
        }

        request = httputil.HTTPServerRequest('GET', '/search?q=42')
        request.remote_ip = '1.1.1.1'
        request._start_time = time.time()
        request.connection = unittest.mock.Mock()

        obj = self.create_application()
        for status, log_level in expectations.items():
            handler = web.RequestHandler(obj, request)
            handler.set_status(status)
            with self.assertLogs(log.access_log) as context:
                obj.log_request(handler)
            self.assertEqual(context.records[0].levelno, log_level)

    def test_that_on_stop_does_not_require_statsd_client(self):
        obj = self.create_application()

        obj.statsd.close()  # don't orphan the client
        obj.statsd = None

        obj.on_stop(obj)

    def test_that_on_stop_does_not_require_available_event(self):
        obj = self.create_application()
        obj._ready_to_serve = None
        obj.on_stop(obj)

    def test_that_setting_status_makes_application_unavailable(self):
        obj = self.create_application()
        self.assertEqual('ok', obj.status)
        obj.status = 'not ok'
        self.assertFalse(obj.available.is_set())

    def test_that_application_is_unavailable_while_stopping(self):
        obj = self.create_application()
        iol = ioloop.IOLoop.current()
        iol.asyncio_loop.run_until_complete(obj.available.wait())
        self.assertTrue(obj.is_available)
        obj.stop(iol)
        self.assertFalse(obj.is_available)


class TestLogConfig(testing.EnvironmentMixin, unittest.TestCase):
    def test_debug_on(self):
        self.setenv('DEBUG', '1')
        config = {{cookiecutter.package_name}}.logging.config()
        self.assertEqual('DEBUG', config['root']['level'])

    def test_debug_off(self):
        self.unsetenv('DEBUG')
        config = {{cookiecutter.package_name}}.logging.config()
        self.assertEqual('INFO', config['root']['level'])
