import cgi

from tornado import testing
import yaml

from {{cookiecutter.package_name}} import app, version


class IndexTests(testing.AsyncHTTPTestCase):
    def get_app(self):
        return app.make_app()

    def test_that_slash_redirects_to_index(self):
        response = self.fetch('/', follow_redirects=False)
        self.assertTrue(
            300 <= response.code < 400,
            f'Expected HTTP redirect status, got {response.code} instead')
        self.assertEqual('/static/index.html', response.headers['Location'])

    def test_that_index_is_html(self):
        response = self.fetch('/')
        self.assertEqual(200, response.code)
        content_type, _ = cgi.parse_header(response.headers['Content-Type'])
        self.assertEqual('text/html', content_type)


class OpenAPITests(testing.AsyncHTTPTestCase):
    def get_app(self):
        return app.make_app()

    def get_httpserver_options(self):
        return {'xheaders': True}

    def get_yaml(self, **kwargs):
        response = self.fetch('/openapi.yaml', **kwargs)
        self.assertEqual(200, response.code)
        return yaml.safe_load(response.body.decode('utf-8'))

    def test_that_openapi_yaml_is_available(self):
        response = self.fetch('/openapi.yaml')
        self.assertEqual(200, response.code)

    def test_that_openapi_contains_service_version(self):
        doc = self.get_yaml()
        self.assertEqual(version, doc['info']['version'])

    def test_that_openapi_spec_targets_appropriate_server(self):
        doc = self.get_yaml()
        self.assertEqual(self.get_url(''), doc['servers'][0]['url'])

        doc = self.get_yaml(headers={'Host': 'doc.example.com'})
        self.assertEqual('http://doc.example.com', doc['servers'][0]['url'])
