from sprockets.http import testing

from {{cookiecutter.package_name}} import app


class ApplicationTestCase(testing.SprocketsHttpTestCase):
    def setUp(self):
        super().setUp()
        self.app.statsd._tcp_reconnect_sleep = 0.1

    def get_app(self):
        self.app = app.make_app()
        return self.app
