import logging
import os
import pathlib


def setup_package():
    logger = logging.getLogger('setup_package')
    my_dir = pathlib.Path(__file__).parent.absolute()
    config = my_dir.parent / 'build' / 'test-environment'
    for line in config.read_text().splitlines():
        if line.startswith('export '):
            line = line[7:]
        line = line.strip()
        name, sep, value = line.partition('=')
        if sep:
            if value.startswith(('"', "'")) and value.endswith(value[0]):
                value = value[1:-1]
            os.environ[name.upper()] = value
            logger.info('set environment variable %s=%s', name.upper(), value)
