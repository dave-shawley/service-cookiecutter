from sprockets.http import testing
from tornado import web

from {{cookiecutter.package_name}} import app
from {{cookiecutter.package_name}}.handlers import helpers


class ReturnStatusHandler(helpers.RequestHandler):
    def get(self):
        self.set_status(int(self.get_query_argument('status', '200')))


class RequestHandlerTests(testing.SprocketsHttpTestCase):
    def get_app(self):
        self.app = app.Application()
        self.app.add_handlers('.*', [web.url('/status', ReturnStatusHandler)])
        return self.app

    def test_that_correlation_id_is_set_in_response(self):
        response = self.fetch('/status')
        self.assertIn('Correlation-ID', response.headers)

    def test_that_correlation_id_is_propagated_from_request(self):
        response = self.fetch('/status', headers={'Correlation-ID': 'testing'})
        self.assertEqual('testing', response.headers['Correlation-ID'])

    def test_that_handler_503s_when_application_is_unavailable(self):
        self.app.available.clear()
        response = self.fetch('/status')
        self.assertEqual(503, response.code)
