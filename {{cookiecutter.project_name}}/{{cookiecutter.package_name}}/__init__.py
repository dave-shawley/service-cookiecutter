import pkg_resources

dist = pkg_resources.get_distribution('{{cookiecutter.project_name}}')
version = dist.version
version_info = version.split('.')

del dist
del pkg_resources
