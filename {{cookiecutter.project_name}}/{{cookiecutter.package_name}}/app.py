import asyncio
import datetime
import os
import pathlib
import pkg_resources

import sprockets.http.app
from sprockets.mixins.metrics import statsd
from sprockets.mixins.mediatype import content, transcoders
from sprockets.mixins import sentry
from tornado import web

from {{cookiecutter.package_name}} import handlers, logging, version


def entry_point():
    """CLI main function"""
    sprockets.http.run(
        make_app,
        {
            'number_of_procs': int(os.environ.get('NUMBER_OF_PROCS', '0')),
            'xheaders': True,
        },
        log_config=logging.config(),
    )


class Application(sprockets.http.app.Application):
    """Application state and configuration.

    Much of the information available for use inside of handlers is
    contained in the ``settings`` dictionary.  The following values
    are available.

    +---------------+---------------------------------------+
    | Setting       | Description                           |
    +===============+=======================================+
    | environment   | Operating environment                 |
    +---------------+---------------------------------------+
    | service       | Name of the service that is running   |
    +---------------+---------------------------------------+
    | user_agent    | User agent string that should be used |
    +---------------+---------------------------------------+
    | version       | Service version                       |
    +---------------+---------------------------------------+

    .. attribute:: info

       Dictionary of application information that is returned
       from the status endpoint.

    """
    def __init__(self, **settings):
        pkg_root = pathlib.Path(
            pkg_resources.resource_filename(__package__, ''))
        settings.setdefault('template_path', str(pkg_root / 'templates'))

        routes = [
            web.url(r'/', web.RedirectHandler, {'url': '/static/index.html'}),
            web.url(r'/openapi.yaml', handlers.OpenAPIHandler),
            web.url(r'/static/(.*)', web.StaticFileHandler,
                    {'path': str(pkg_root / 'static')}),
            web.url(r'/status', handlers.StatusHandler),
        ]
        settings.update({
            'environment': os.environ.get('ENVIRONMENT', 'development'),
            'log_function': logging.log_request,
            'service': '{{cookiecutter.project_name}}',
            'user_agent': f'{{cookiecutter.project_name}}/{version}',
            'version': version,
        })

        super().__init__(routes, **settings)

        if self.settings['environment'] not in ('staging', 'production'):
            os.environ.pop('SENTRY_DSN', None)

        self.json_transcoder = transcoders.JSONTranscoder()
        content.install(self, 'application/json')
        content.add_transcoder(self, self.json_transcoder)

        self.info = {
            'application': self.settings['service'],
            'environment': self.settings['environment'],
            'started_at': datetime.datetime.now(datetime.timezone.utc),
            'status': 'starting',
            'version': self.settings['version'],
        }

        self._ready_to_serve = None  # created in before_run
        self.before_run_callbacks.append(self.before_run)
        self.on_shutdown_callbacks.append(self.on_stop)

    @property
    def is_available(self) -> bool:
        """Can the application serve requests?"""
        if self._ready_to_serve is None:
            return False
        return self._ready_to_serve.is_set()

    @property
    def available(self):
        """:class:`asyncio.Event` representing the applications availability.

        Check this event if you need to know whether the application is
        available for processing requests.

        :rtype: asyncio.Event

        """
        return self._ready_to_serve

    @property
    def status(self):
        """Application's operational status.

        This is the human-readable application status.  It is a reflection
        of :attr:`Application.available`.  If the application is available,
        then the status is "ok".

        If the status is set to anything other than "ok", then the
        application will be marked as unavailable.

        """
        return self.info['status']

    @status.setter
    def status(self, new_status):
        self.logger.info('transitioning from %s -> %s', self.info['status'],
                         new_status)
        self.info['status'] = new_status
        if new_status == 'ok':
            self._ready_to_serve.set()
        else:
            self._ready_to_serve.clear()

    @staticmethod
    def before_run(self, io_loop):
        """Called synchronously before the HTTP server starts.

        :param Application self: the application instance
        :param tornado.ioloop.IOLoop io_loop: ignored

        """
        self._ready_to_serve = asyncio.Event()
        statsd.install(self,
                       protocol=os.environ.get('STATSD_PROTOCOL', 'tcp'),
                       namespace='applications.{service}.{environment}'.format(
                           **self.settings))
        sentry.install(
            self,
            environment=self.settings['environment'],
            include_paths=[__package__],
            release=self.settings['version'],
        )
        self.status = 'ok'

    @staticmethod
    def on_stop(self):
        """Called synchronously after the HTTP server is stopped.

        :param Application self: the application instance

        """
        if self._ready_to_serve is not None:
            self._ready_to_serve.clear()
        statsd_client = getattr(self, 'statsd', None)
        if statsd_client:
            statsd_client.close()


def make_app(**settings):
    return Application(**settings)
