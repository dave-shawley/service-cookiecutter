import contextvars
import datetime
import distutils.util
import logging
import os

import tornado.log

from {{cookiecutter.package_name}} import handlers

context = contextvars.ContextVar('logging_context')


class LogContextInjector(logging.Filter):
    def filter(self, record):
        log_context = context.get({})
        record.__dict__.update(log_context)
        record.__dict__.setdefault('correlation_id', '-')
        return True


def config():
    debug = bool(distutils.util.strtobool(os.environ.get('DEBUG', 'no')))
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'log-context-injector': {
                '()': LogContextInjector,
            },
        },
        'formatters': {
            'human-readable': {
                'datefmt': '%Y-%m-%dT%H:%M:%S',
                'format': ('%(asctime)s.%(msecs)03d %(levelname)-10s %(name)s:'
                           ' %(message)s (CID:%(correlation_id)s)'),
            },
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'filters': ['log-context-injector'],
                'formatter': 'human-readable',
                'stream': 'ext://sys.stdout',
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'DEBUG' if debug else 'INFO',
        },
    }


def log_request(handler):
    """Customized access log function.

    :param tornado.web.RequestHandler handler:

    """
    status = handler.get_status()
    if isinstance(handler, handlers.StatusHandler) and status < 400:
        return

    if status < 400:
        log_level = logging.INFO
    elif status < 500:
        log_level = logging.WARNING
    else:
        log_level = logging.ERROR

    started_at = datetime.datetime.fromtimestamp(handler.request._start_time,
                                                 datetime.timezone.utc)
    try:
        bytes_written = handler.response_bytes_written
    except AttributeError:
        bytes_written = '-'

    tornado.log.access_log.log(
        log_level,
        '%s %s %s [%s] "%s %s %s" %d %s "%s" "%s" (secs:%.03f)',
        handler.request.remote_ip,
        '-',  # RFC-1413 user identifier
        handler.get_current_user() or '-',
        started_at.strftime('%d/%b/%Y:%H:%M:%S %z'),
        handler.request.method,
        handler.request.uri,
        handler.request.version,
        status,
        bytes_written,
        handler.request.headers.get('Referer', '-'),
        handler.request.headers.get('User-Agent', '-'),
        handler.request.request_time(),
    )
