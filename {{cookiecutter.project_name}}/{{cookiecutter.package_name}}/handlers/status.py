from . import helpers


class StatusHandler(helpers.RequestHandler):
    def on_unavailable(self):
        self.set_status(503)

    def get(self):
        return self.send_response(self.application.info)
