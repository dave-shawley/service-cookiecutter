import asyncio
import logging
import uuid

from sprockets.mixins.mediatype import content
from sprockets.mixins.metrics import statsd
from tornado import web

import {{cookiecutter.package_name}}.app
import {{cookiecutter.package_name}}.logging


class ResponseSizeTracker(web.RequestHandler):
    """Keep track of bytes written so that we can use it later.

    Mix this in over :class:`~tornado.web.RequestHandler` to report
    the number of bytes written in our access logger.

    """
    def initialize(self, **kwargs):
        super().initialize(**kwargs)
        self.__bytes_written = 0

    @property
    def response_bytes_written(self):
        return self.__bytes_written

    def flush(self, include_footers=False):
        for chunk in self._write_buffer:
            self.__bytes_written += len(chunk)
        return super().flush(include_footers)


class ContextLogger(web.RequestHandler):
    log_context: dict
    logger: logging.Logger

    def initialize(self, **kwargs):
        super().initialize(**kwargs)
        self.log_context = {}
        {{cookiecutter.package_name}}.logging.context.set(self.log_context)
        self.logger = logging.getLogger(self.__class__.__name__)


class RequestHandler(content.ContentMixin, statsd.StatsdMixin, ContextLogger,
                     ResponseSizeTracker, web.RequestHandler):
    """Basic request handler.

    This is the base class for all request handlers.  It contains
    functionality and behavior that is available everywhere.

    """
    application: '{{cookiecutter.package_name}}.app.Application'

    def __init__(self, *args, **kwargs):
        # this needs to be set NEFORE super.__init__ since it WILL
        # BE referred to from withing set_default_headers during
        # super().__init()
        self.correlation_id = str(uuid.uuid4())
        super().__init__(*args, **kwargs)

    async def prepare(self):
        """Call :meth:`.on_unavailable` if the application is not available."""
        maybe_coro = super().prepare()
        if asyncio.iscoroutine(maybe_coro):  # pragma: no cover
            await maybe_coro

        self.correlation_id = self.request.headers.get('Correlation-ID',
                                                       self.correlation_id)
        self.log_context['correlation_id'] = self.correlation_id
        self.set_header('Correlation-ID', self.correlation_id)

        if not self.application.is_available:
            self.on_unavailable()

    def set_default_headers(self):
        super().set_default_headers()
        self.set_header('Correlation-ID', self.correlation_id)

    def on_unavailable(self):
        """Called from prepare when not the application is not available.

        The default behavior is to set the status to 503 and complete the
        request.  In other words, if the application is not available, then
        the request handler will not be invoked after :meth:`.prepare` is
        complete.

        """
        self.set_status(503)
        self.finish()
