from . import helpers


class OpenAPIHandler(helpers.RequestHandler):
    def get(self):
        return self.render('openapi.yaml',
                           json_encode=self.application.json_transcoder.dumps)

    def on_unavailable(self):
        """This handler is always available."""
        pass
