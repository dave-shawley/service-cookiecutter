#!/usr/bin/env python
import setuptools

setuptools.setup(python_requires='>=3.7.1',
                 setup_requires=['setuptools>=40.6'])
