{{ cookiecutter.project_name|title() }}
{{ '=' * cookiecutter.project_name|length() }}

{{cookiecutter.short_description}}

.. include:: ../README.rst

.. toctree::
   :hidden:

   implementation
   changelog
   contributing
