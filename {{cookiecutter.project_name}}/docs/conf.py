import {{cookiecutter.package_name}}

project = '{{cookiecutter.project_name}}'
author = 'AWeber Communications, Inc.'
copyright = '2019, AWeber Communications, Inc.'
version = {{cookiecutter.package_name}}.version
needs_sphinx = '2.0'
extensions = []
html_static_path = ['.']

# see https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html
extensions.append('sphinx.ext.intersphinx')
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'tornado': ('https://www.tornadoweb.org/en/stable/', None),
}

# see https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
extensions.append('sphinx.ext.autodoc')

# see https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html
extensions.append('sphinx.ext.extlinks')
extlinks = {
    'compare': ('https://gitlab.aweber.io/{{cookiecutter.project_team}}/services'
                '/{{cookiecutter.project_name}}/compare/%s', ''),
    'jira': ('https://jira.aweber.io/browse/%s', ''),
}
