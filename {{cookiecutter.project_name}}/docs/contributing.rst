=================
Contributing back
=================

Code standards
==============

Basic requirements
------------------
- Python source code follows :pep:`8` as enforced by `flake8`_
- Python source code is formatted using the `yapf`_ utility
- Tests cover 100% of source code lines
- Tests cover 100% of logic branches
- The `bandit`_ utility is used to detect static security vulnerabilities
- The `safety`_ utility is used to detect vulnerable packages & libraries

Modifications that violate any of the Standards will be rejected.  The easiest
way to check this is to run the *./ci/test* script.  If it fails, then you need
to fix whatever is reported.

Other expectations
------------------
- The release history (*docs/changelog.rst*) is up to date


Quick start
===========

Create environment
------------------
.. code-block:: bash

   $ python3 -mvenv env
   $ . ./env/bin/activate
   (env) $ pip install -qe .
   (env) $ pip install -q '.[dev]'

Run tests
---------
.. code-block:: bash

   (env) $ ./ci/test

Build documentation suite
-------------------------
.. code-block:: bash

   (env) $ ./ci/docs

Build source distribution
-------------------------
.. code-block::  bash

   (env) $ ./ci/package

Running application
-------------------
.. code-block:: bash

   (env) $ {{cookiecutter.project_name}}

Note that this will complain since there is no statsd service running but the
service will run correctly.

Installing yapf commit hook
---------------------------
This project uses yapf_ to ensure consistent code formatting.  The *ci*
directory contains a git pre-commit hook that runs yapf over Python files
before they are committed.  Install it using a symlink::

    $ ln -s $(pwd)/ci/pre-commit-hook .git/hooks/pre-commit

.. _bandit: https://bandit.readthedocs.io/en/latest/
.. _flake8: https://flake8.readthedocs.io/en/latest/
.. _safety: https://github.com/pyupio/safety
.. _yapf: https://github.com/google/yapf
