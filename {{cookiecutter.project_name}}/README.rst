Configuration
=============

+--------------------+-----------------+-----------------------------------------------+
| Name               | Default         | Description                                   |
+====================+=================+===============================================+
| CONSUL_HOST        | see below (1)   | Identifies the Consul Key-Value host          |
+--------------------+-----------------+-----------------------------------------------+
| ENVIRONMENT        | development     | Identifies the environment that the service   |
|                    |                 | is running it.  This affects metric paths     |
|                    |                 | and functionality.                            |
+--------------------+-----------------+-----------------------------------------------+
| PREFIX             | see below (2)   | Path prefix in the Consul KV for the service  |
+--------------------+-----------------+-----------------------------------------------+
| SENTRY_DSN         | *unset*         | If configured, sentry reports will be issued  |
|                    |                 | using this value for uncaught exceptions.     |
+--------------------+-----------------+-----------------------------------------------+
| STATSD_HOST        | 127.0.0.1       | Statsd server to send metrics to              |
+--------------------+-----------------+-----------------------------------------------+
| STATSD_PORT        | 8125            | TCP port to send metrics to                   |
+--------------------+-----------------+-----------------------------------------------+

.. rubric:: Notes

(1) the default consul host uses the ``ENVIRONMENT`` environment variable
    in the hostname -- ``consul.service.$ENVIRONMENT.consul``
(2) the default prefix is currently::

      /services/{{cookiecutter.project_team}}/services/{{cookiecutter.project_name}}

    This value **MUST** match the SCM path for the service.

Developer quick start
=====================
See *docs/contributing.rst* for more detailed developer instructions.  The
following snippet sets up the environment and runs tests.

::

   $ python3 -mvenv env
   $ ./ci/test
   Running flake8...
   Running yapf...
   Running bandit...
   Running safety...
   Running tests...
   ..................
   Name                               Stmts   Miss Branch BrPart  Cover   Missing
   ------------------------------------------------------------------------------
   {{cookiecutter.package_name}}/__init__.py{{ ' ' * (37 - cookiecutter.package_name|length() - 12) }}  9      0      0      0   100%
   {{cookiecutter.package_name}}/app.py{{ ' ' * (37 - cookiecutter.package_name|length() - 7) }} 34      0      2      0   100%
   ------------------------------------------------------------------------------
   TOTAL                                 43      0      2      0   100%
   ----------------------------------------------------------------------
   Ran 18 tests in 0.486s

   OK
